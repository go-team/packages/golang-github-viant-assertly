Source: golang-github-viant-assertly
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders:
 Aloïs Micard <creekorful@debian.org>,
 Roger Shimizu <rosh@debian.org>
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-golang,
 golang-any,
 golang-github-stretchr-testify-dev,
 golang-github-viant-toolbox-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-viant-assertly
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-viant-assertly.git
Homepage: https://github.com/viant/assertly
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/viant/assertly

Package: golang-github-viant-assertly-dev
Architecture: all
Depends:
 golang-github-stretchr-testify-dev,
 golang-github-viant-toolbox-dev,
 ${misc:Depends}
Description: Arbitrary datastructure validation golang library
 Data structure testing library (assertly) for Go.
 .
 This library enables complex data structure testing, specifically:
   1. Realtime transformation or casting of incompatible data types
      with directives system.
   2. Consistent way of testing of unordered structures.
   3. Contains, Range, RegExp support on any data structure deeph level.
   4. Switch case directive to provide expected value alternatives based
   on actual switch/case input match.
   5. Macro system enabling complex predicate and expression evaluation,
 and customization.
 .
 Motivation This library has been created as a way to unify original
 testing approaches introduced to https://github.com/viant/dsunit
 and https://github.com/viant/endly
